public class Main {
    public static double find_fibonacci(int n){
        //The Fibonacci Binet's formula
        double phi = (1+Math.sqrt(5))/2;
        return Math.round((Math.pow(phi,n)-Math.pow((1-phi),n))) / Math.sqrt(5);
    }

    public static int fibonacci_lowerbound(double N, int min, int max){
        int pivot = (min+max)/2;
        if(min == pivot){
            return pivot;
        }
        if(find_fibonacci(pivot)<=N){
            return fibonacci_lowerbound(N,pivot,max);
        }
        else
            return fibonacci_lowerbound(N,min,pivot);
    };

    public static void calculate(int n){
        int l_bound = fibonacci_lowerbound(n,0,1000);
        System.out.print(Math.round(find_fibonacci(l_bound)));
        System.out.print("  <---->   ");
        System.out.printf("%.0f", find_fibonacci(l_bound+1));
        System.out.println("\n########################");

    }

    public static void main(String[] args) {
        calculate(1044);
        calculate(8999913);
        calculate(7);
        calculate(67);
    }
}
